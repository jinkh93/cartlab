//
//  TableViewController.swift
//  cartlab
//
//  Created by Jin Hong on 2018-12-20.
//  Copyright © 2018 junk. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift
import RxCocoa
import RxDataSources

struct Section: SectionModelType, AnimatableSectionModelType {
    typealias Identity = String
    typealias Item = Int
    let title: String
    private(set) var items: [Item]
    init(title: String, items: [Item]) {
        self.title = title
        self.items = items
    }
    init(original: Section, items: [Int]) {
        self = original
        self.items = items
    }
    var identity: String { return title }
}



class TableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var _sections = [
        Section(title: "one", items: [1,2,3]),
        Section(title: "two", items: [4,5,6]),
        Section(title: "three", items: [7,8,9]),
        ]
    var sections: Variable<[Section]> = Variable([])
    
    private let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(Cell.self, forCellReuseIdentifier: "Cell")
        let ds = RxTableViewSectionedAnimatedDataSource<Section>(
            configureCell: cellConfiguration,
            titleForHeaderInSection: titleForHeaderInSection
        )
        sections.asObservable().subscribe { (event) in
            print("PRINTING", event)
        }
        sections.asObservable().bind(to: tableView.rx.items(dataSource: ds)).disposed(by: bag)
    }
    
    private func cellConfiguration(dataSource: TableViewSectionedDataSource<Section>, tableView: UITableView, indexPath: IndexPath, item: Section.Item) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = "\(item)"
        return cell
    }
    
    private func titleForHeaderInSection(dataSource: TableViewSectionedDataSource<Section>, section: Int) -> String? {
        return sections.value[section].identity
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.sections.value = self._sections
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.sections.value.append(.init(title: "four", items: [10,11,12]))
        }
    }
}
