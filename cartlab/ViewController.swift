//
//  ViewController.swift
//  cartlab
//
//  Created by Jin Hong on 2018-12-18.
//  Copyright © 2018 junk. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift
import RxCocoa
import RxDataSources

struct Thing {
    let id: String
}

struct SomethingViewModel {
    var d2 = BehaviorSubject<[Thing]>(value: [])
    var data = Observable.from([
        Thing(id: "1"),
        Thing(id: "2"),
        Thing(id: "3"),
        Thing(id: "4"),
        Thing(id: "5"),
        Thing(id: "6"),
        ])
    var items: [Item] = []
}
extension SomethingViewModel: SectionModelType {
    typealias Item = Thing
    init(original: SomethingViewModel, items: [Thing]) {
        self = original
        self.items = items
    }
}

class Cell: UITableViewCell {
    
}

class ViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!

    private let container = Container()
    private(set) var something: Something?
    private(set) var scheduler: MainScheduler?
    private(set) var disposeBag: DisposeBag?
    private(set) var viewModel: SomethingViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(Cell.self, forCellReuseIdentifier: "Cell")
        
        // Do any additional setup after loading the view, typically from a nib.
        container.autoregister(Something.self, initializer: _Something.init)
        container.register(MainScheduler.self) { _ in return .instance }
        container.autoregister(DisposeBag.self, initializer: DisposeBag.init)
        container.register(SomethingViewModel.self) { _ in return SomethingViewModel() }
        
        resolveDependencies(from: container)
        
        bindViewModelToTableView()
        subscribeTableViewItemSelection()
        createAndBindRightBarButton()
    }
    
    func bindViewModelToTableView() {
        viewModel?.d2.bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: Cell.self)) { row, element, cell in
            cell.textLabel?.text = "ROW: \(row), ELEM: \(element.id)"
            }.disposed(by: disposeBag!)
    }
    
    func subscribeTableViewItemSelection() {
        tableView.rx.itemSelected.subscribe { event in
            print(event)
            }.disposed(by: disposeBag!)
    }
    
    func createAndBindRightBarButton() {
        let navi = UIBarButtonItem(title: "Table", style: .plain, target: nil, action: nil)
        navi.rx.tap
            .debounce(0.2, scheduler: scheduler!)
            .bind { [weak self] _ in self?.performSegue(withIdentifier: "table", sender: nil) }
            .disposed(by: disposeBag!)
        navigationItem.rightBarButtonItem = navi
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        something?.doSomething()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.viewModel?.d2.onNext([
                Thing(id: "1"),
                Thing(id: "2"),
                Thing(id: "3"),
                Thing(id: "4"),
                Thing(id: "5"),
                Thing(id: "6"),
                ])
        }
    }
    
    private func resolveDependencies(from container: Container) {
        something = container.resolve(Something.self)
        scheduler = container.resolve(MainScheduler.self)
        disposeBag = container.resolve(DisposeBag.self)
        viewModel = container.resolve(SomethingViewModel.self)
    }
}

protocol Something {
    func doSomething()
}
class _Something: Something {
    init() {}
    func doSomething() {
        print("WHAT UP BITCHES")
    }
}
