//
//  AppDelegate.swift
//  cartlab
//
//  Created by Jin Hong on 2018-12-18.
//  Copyright © 2018 junk. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift
import RxCocoa
import RxDataSources

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
